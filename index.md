---
layout: default_sidebar
paginate: false
---

<ul class="post-list">
    {% for category in site.portal_categories %}
        <div class="widget kopa-entry-list-widget clearfix">
            <h4 class="widget-title clearfix">
                <span class="title-text">{{ category}}</span>
            </h4>
            {% for post in site.categories[category] limit:2 %} 
                    {% include post_row.html %}
            {% endfor %}
        </div>
    {% endfor %}
</ul>