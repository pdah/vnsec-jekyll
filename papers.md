---
title: Papers
author: suto
layout: page
tweetbackscheck:
  - 1408358840
shorturls:
  - 'a:0:{}'
aktt_notify_twitter:
  - no
kopa_newsmixlight_total_view:
  - 17
---
Papers & presentations published by VNSECURITY members. Copyright and all rights therein are maintained by the authors or by other copyright holders.

### 2013

*   Le Dinh Long, *Exploiting nginx chunked overflow bug, the undisclosed attack vector*, Presentation, <a href="http://www.secuinside.com" target="_blank">SECUINSIDE</a>, Jul 2013.
*   Nguyen Anh Quynh, *OptiCode: Machine Code Deobfuscation for Malware Analysis*, Presentation, <a href="http://syscan.org" target="_blank">SysCan SG</a>, Apr 2013.
*   Nguyen Anh Quynh, *Phát hiện virus đa hình với chữ ký ngữ nghĩa*, Presentation, <a href="http://tetcon.org" target="_blank">TetCon</a>, Jan 2013.

### 2012

*   Le Dinh Long & Thanh Nguyen, *PEDA: Polishing Up GDB for Exploit Development*, Workshop, <a href="http://conference.hitb.org/hitbsecconf2012kul/thanh-nguyen/" target="_blank">Hack in The Box</a>, October 2012
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">Thai Duong & Juliano Rizzo, <em>The CRIME attack</em>, Presentation, <a href="http://www.ekoparty.org/2012/juliano-rizzo.php" target="_blank">Ekoparty</a>, September 2012</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">Le Dinh Long, <em>Linux Interactive Exploit Development with GDB abd PEDA</em>, Workshop, <a href="http://www.blackhat.com/html/bh-us-12/bh-us-12-briefings.html#Le" target="_blank">Black Hat USA</a>, August 2012</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">Thanh Nguyen, <em>Cyber Warfare</em>, Keynote, <a href="http://tetcon.org" target="_blank">Tetcon</a>, Jan 2012.</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">Thai Duong, </span><span style="color: #000000;font-style: normal;font-weight: normal"><em>Những lỗ hổng ít được biết đến và cách phòng chống</em></span><span style="color: #000000;font-style: normal;font-weight: normal">, Presentation, <a href="http://tetcon.org" target="_blank">Tetcon</a>, Jan 2012. (<a href="https://docs.google.com/present/view?id=dffw6v5g_31vc4jsjg3&revision=_latest&start=0&theme=shelley&cwj=true">slides</a>)</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal">Pham Van Toan, <em>Phát hiện lỗi phần mềm thông qua kỹ thuật fuzzing thông minh</em>, Presentation, <a href="http://tetcon.org" target="_blank">Tetcon</a>, Jan 2012. (<a href="https://docs.google.com/viewer?a=v&pid=explorer&chrome=true&srcid=0B_L6MdkbAn4MNTQxNDI3ODAtMjhjZi00MzMxLWJmNTAtMTUwOTJjOWNmMDVj&hl=en_US">slides</a>) </span></em></span>**

### 2011

*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">KaiJern Lau, <em>Malware Sandboxing the Xandora Way</em>, Presentation, <a href="http://conference.hitb.org/hitbsecconf2011kul/?page_id=2048" target="_blank">Hack in The Box Malaysia</a>, October 2011.</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><span style="color: #000000;font-style: normal;font-weight: normal">Thai Duong & Juliano Rizzo, </span><span style="color: #000000;font-style: normal;font-weight: normal"><em>BEAST: Surprising crypto attack against HTTPS</em></span><span style="color: #000000;font-style: normal;font-weight: normal">, Presentation, <a href="http://www.ekoparty.org/2011/thai-duong.php" target="_blank">Ekoparty</a>, Sept 2011. (<a href="http://www.insecure.cl/Beast-SSL.rar" target="_blank">paper</a>)</span></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal">Le Dinh Long & Thanh Nguyen, <em>ARM Exploitation ROPmap</em>, Presentation, <a href="http://www.blackhat.com/html/bh-us-11/bh-us-11-briefings.html#Le" target="_blank">Black Hat USA</a>, August 2011 and <a href="http://pacsec.jp/speakers.html" target="_blank">PacSec Japan</a>, November 2011. (<a href="https://media.blackhat.com/bh-us-11/Le/BH_US_11_Le_ARM_Exploitation_ROPmap_Slides.pdf">slides</a>)</span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><strong><span style="color: #000000;font-style: normal;font-weight: normal">Thai Duong & Juliano Rizzo, </span><span style="color: #000000;font-style: normal;font-weight: normal"><em>Cryptography in the Web: The Case of Cryptographic Design Flaws in ASP.NET</em></span><span style="color: #000000;font-style: normal;font-weight: normal">, Technical Paper, <a href="http://www.ieee-security.org/TC/SP2011/program.html" target="_blank">IEEE Symposium on Security & Privacy</a>, May 2011. (<a href="http://www.ieee-security.org/TC/SP2011/PAPERS/2011/paper030.pdf" target="_blank">paper</a>)</span></strong></span></em></span>**

### 2010

*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal">Le Dinh Long & Thanh Nguyen, <em>Payload already inside: data re-use for ROP exploits</em>, Presentation, <a href="http://conference.hitb.org/hitbsecconf2010kl/" target="_blank">Hack in The Box Malaysia</a>, October 2010 and <a href="https://deepsec.net/schedule" target="_blank">DeepSec Austria</a>, November 2010. (<a href="http://force.vnsecurity.net/download/DeepSec_Slides_Payload_already_inside_data_reuse_for_ROP_exploits.pdf" target="_blank">slides</a>)</span></em></span>**
*   **<span style="color: #ff0000"><span style="color: #000000;font-style: normal;font-weight: normal">Nguyen Anh Quynh, </span><span style="color: #000000;font-style: normal;font-weight: normal"><em>Detecting Malicious Documentations</em></span><span style="color: #000000;font-style: normal;font-weight: normal">, </span></span>**Presentation, <a href="http://www.syscan.org/index.php/archive/view/year/2010/city/hcm/pg/speakers" target="_blank">SyScan HCM</a>, September 2010.
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal"><strong><span style="color: #000000;font-style: normal;font-weight: normal">Thai Duong & Juliano Rizzo, </span><span style="color: #000000;font-style: normal;font-weight: normal"><em>Padding Oracles Everywhere</em></span><span style="color: #000000;font-style: normal;font-weight: normal">, Presentation, <a href="http://www.ekoparty.org/thai-duong-2010.php" target="_blank">Ekoparty</a>, September 2010.</span></strong></span></em></span>**
*   **<span style="color: #ff0000"><em><span style="color: #000000;font-style: normal;font-weight: normal">Nguyen Anh Quynh, <em>Operating System Fingerprinting for Virtual Machines</em>, Presentation, <a href="https://www.defcon.org/html/defcon-18/dc-18-schedule.html" target="_blank">Defcon 18</a>, July 2010 and </span></em></span>**[SyScan Taipei][1], August 2010. (<a href="http://www.defcon.org/images/defcon-18/dc-18-presentations/Quynh/DEFCON-18-Quynh-OS-Fingerprinting-VM.pdf" target="_blank">slides</a>)
*   ****Nguyen Anh Quynh, *Virt-ICE: next generation debugger for malware analysis*, White paper and Presentation, <a href="http://www.blackhat.com/html/bh-us-10/bh-us-10-briefings.html" target="_blank">Black Hat USA</a>, July 2010. ([papers][2], [slides][3])  
    ****
*   ****Le Dinh Long, *Payload already inside: data re-use for ROP exploits*, White paper and Presentation, <a href="http://www.blackhat.com/html/bh-us-10/bh-us-10-briefings.html" target="_blank">Black Hat USA</a>, July 2010 and <a href="http://www.syscan.org/index.php/archive/view/year/2010/city/hcm/pg/speakers" target="_blank">SyScan HCM</a>, September 2010. ([papers][4], [slides][5])
*   Thai Duong & Juliano Rizzo, *Practical Crypto Attacks Against Web Applications*, White paper and Presentation, <a href="http://www.blackhat.com/html/bh-eu-10/bh-eu-10-briefings.html#Duong" target="_blank">Black Hat Europe</a>, April 2010 and [USENIX Workshop on Offensive Technologies][6], August 2010. (<a href="https://media.blackhat.com/bh-eu-10/whitepapers/Duong_Rizzo/BlackHat-EU-2010-Duong-Rizzo-Padding-Oracle-wp.pdf" target="_blank">papers</a>, <a href="https://media.blackhat.com/bh-eu-10/presentations/Duong_Rizzo/BlackHat-EU-2010-Duong-Rizzo-Padding-Oracle-slides.pdf" target="_blank">slides</a>)

### 2009

*   Nguyen Anh Quynh, *eKimono: A Malware Scanner for Virtual Machines*, Presentation, <a href="https://deepsec.net/docs/speaker.html" target="_blank">DeepSec Austria</a>, November 2009 and <a href="http://conference.hitb.org/hitbsecconf2009kl/?page_id=482" target="_blank">Hack in The Box Malaysia</a>, October 2009. ([slides][7])
*   Thai Duong & Juliano Rizzo, *Flickr’s API Signature Forgery Vulnerability*, Technical paper, September 2009. ([paper][8])
*   Nguyen Anh Quynh, *Memory forensic and incident response for live virtual machine (VM)*, Presentation, <a href="http://www.frhack.org/frhack-conference.php#virtual-machine-memory-forensic" target="_blank">FRHACK France</a>, September 2009.
*   Nguyen Anh Quynh, *Detecting rootkits inside Virtual Machine*, Presentation, <a href="http://xcon.xfocus.org/speakers.html" target="_blank">XCON China</a>, August 2009.
*   Nguyen Anh Quynh, *Outspect: Live Memory Forensic and Incident Response for Virtual Machine*, Presentation, <a href="http://www.syscan.org/Sg/program2.html" target="_blank">Syscan Singapore</a>, July 2009.

### 2007

*   Nguyen Anh Quynh, *Hijacking Virtual Machine Execution*, Presentation, <a href="http://www.blackhat.com/html/bh-japan-07/bh-jp-07-en-speakers.html#Anh" target="_blank">Black Hat Japan</a>, October 2007.
*   Thanh Nguyen (Red Dragon) and Mikado, *Cheating Massively Multiplayer Online Game for Fun and Profit*, Presentation, <a href="http://conf.vnsecurity.net" target="_blank">VNSECON</a>, July 2007.
*   Nguyen Anh Quynh *Hijacking Virtual Machine Execution for Fun and Profit, *Presentation, [VNSECON][9], July 2007 and [Bellua Cyber Security Asia][10], October, 2008 and [DeepSec Austria][11], December 2007. ([slides][12])
*   Nam Nguyen, *Unintrusive Back in Time Java Tracers: Methods and Practices*, Presentation, [VNSECON,][9] July 2007.
*   Thai Duong, *Zombilizing The Web Browsers Via Flash Player 9, *Presentation* *[VNSECON,][9] July 2007.
*   Thanh Nguyen (Red Dragon), *UPnP Hacking &#8211; You Plug n Pray, *Presentation*, *[VNSECON,][9] July 2007.

### 2006

*   Nguyen Anh Quynh, *XEBEK: A Next Generation Honeypot Monitoring System*, Presentation, <a href="http://eusecwest.com/pastevents.html" target="_blank">EUSecWest Amsterdam</a>, 2006. ([slides][13])
*   Nguyen Anh Quynh, *Towards an Invisible Honeypot Monitoring System*, Presentation, <a href="http://conference.hitb.org/hitbsecconf2009kl/?page_id=482" target="_blank">Hack in The Box Malaysia</a>, September 2006 and [Hack.lu Luxembourg][14], October 2006. ([video][15], [slides][16])

### 2003

*   Thanh Nguyen (Red Dragon), [ *Advanced Linux Kernel Keylogger*][17], Presentation, <a href="http://conference.hackinthebox.org/hitbsecconf2003/speakers.php#rd" target="_blank">Hack In The Box Malaysia</a>, December 2003.

### 2002

*   Thanh Nguyen (Red Dragon), *Linux Kernel Keylogger, *Technical paper, <a href="http://phrack.org/issues.html?issue=59&id=14#article" target="_blank">Phrack Magazine #59</a>, June 2002.

<div id="_mcePaste" style="overflow: hidden;width: 1px;height: 1px">
  <h2>
    Virt-ICE: next generation debugger for malware analysis
  </h2>
</div>

 [1]: http://www.syscan.org/Tpe/index.php
 [2]: https://media.blackhat.com/bh-us-10/whitepapers/Anh/BlackHat-USA-2010-Anh-Virt-ICE-wp.pdf
 [3]: https://media.blackhat.com/bh-us-10/presentations/Anh/BlackHat-USA-2010-Anh-Virt-ICE-slides.pdf
 [4]: http://force.vnsecurity.net/download/longld/BHUS10_Paper_Payload_already_inside_data_reuse_for_ROP_exploits.pdf
 [5]: http://force.vnsecurity.net/download/longld/BHUS10_Slides_Payload_already_inside_data_reuse_for_ROP_exploits_v1.pdf
 [6]: http://www.usenix.org/events/woot10/tech/
 [7]: http://conference.hitb.org/hitbsecconf2009kl/materials/D1T2%20-%20Nguyen%20Anh%20Quynh%20-%20eKimono%20.pdf
 [8]: http://netifera.com/research/flickr_api_signature_forgery.pdf
 [9]: http://conf.vnsecurity.net/
 [10]: http://www.bellua.com/bcs/asia07.schedule.html
 [11]: http://2007.deepsec.net
 [12]: http://www.bellua.com/bcs/asia07.materials/nguyen_anh_quynh.pdf
 [13]: http://eusecwest.com/esw06/esw06-nguyen.ppt
 [14]: http://2009.hack.lu/archive/2006/Xebek-HackDotLu06.pdf
 [15]: http://video.google.com/videoplay?docid=60467028831215862&ei=rJ2oS5C3IZS2wgPotvT2CA&q=hitbsecconf2006+owards+an+Invisible+Honeypot+Monitoring+System&view=3#
 [16]: http://www.packetstormsecurity.org/hitb06/DAY_2_-_Nguyen_Anh_Quynh_-_Towards_an_Invisible_Honeypot_Monitoring_Tool.pdf
 [17]: http://www.archive.org/details/hitb2003-Red-Dragon