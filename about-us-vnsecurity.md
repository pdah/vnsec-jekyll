---
title: VNSECURITY
author: admin
layout: default_sidebar
---

## VNSECURITY

***

**VNSECURITY is the pioneer and leading security research group in Vietnam since 1998.**

VNSECURITY, founded since 1998 (formerly VISG), is a team made up of various members and contributors throughout the security community. Each member has a unique skill set that contributes to the team. Our team consists of open-minded and friendly people that enjoy chatting, laughing and working together.

## Team Members

### Co-Founders

* MrChuoi (Dao Hai Lam)
* RD / Red Dragon (Thanh Nguyen)
* Xichzo / SeekZero (Le Dinh Long)

### Active Members

* Bluemood / pmquan
* Chairuou
* Conmale
* Computer_Angel / NeoZ
* Duong Ngoc Thai (thaidn)
* Eugene Teo
* KaiJern Lau (Xwings)
* Leenmie
* Lukas
* Mikado
* Mybb
* Nguyen Thanh Nam (Lamer)
* Nguyen Anh Quynh
* Olalalili
* SuperKhung
* Suto

### Inactive Members

* Convit
* Getfly
* Nm

***NOTES: We have no relation with any other website called themself VNSECURITY such as vnsecurity.com (VHF), vnsecurity.vn, vnsecurity.info, …VNSECURITY.NET is the only domain belongs to VNSECURITY team since 2000.***

***GHI CHÚ: Chúng tôi không có bất kỳ quan hệ nào với các website có tên VNSECURITY khác được thành lập sau này như vnsecurity.com (VHF), vnsecurity.vn, vnsecurity.info, … Tên miền VNSECURITY.NET là tên miền duy nhất được sử dụng bới nhóm VNSECURITY từ năm 2000.***
